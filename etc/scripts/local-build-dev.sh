#!/usr/bin/env bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then 
    cd $SCRIPT_PATH
fi
cd ../..

if [ -d "target" ]
then
  rm -rf target
fi

yarn deploy:development
app_version=`yarn -s version:current`
ansible-playbook -i etc/ansible/inventories/localhost etc/ansible/configure.yml
ansible-playbook -i etc/ansible/inventories/localhost etc/ansible/build.yml --extra-vars "{'app_version': '$app_version', 'profile': development, 'push': false}"
