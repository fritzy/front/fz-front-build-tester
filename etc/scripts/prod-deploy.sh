#!/usr/bin/env bash
SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then 
    cd $SCRIPT_PATH
fi
cd ../..

eval $(ssh-agent -s)
ssh-add ~/.ansible/ansible

export HCLOUD_TOKEN="$(ansible-vault view etc/ansible/inventories/production/hcloud-token.vault)"
ansible-galaxy install -r etc/ansible/requirements.yml
ansible-playbook -i etc/ansible/inventories/production etc/ansible/deploy.yml --extra-vars "environment_name=production"
