#!/usr/bin/env sh

mkdir -p $PWD/target
mkdir -p $PWD/node_modules

sudo mount -t tmpfs -o rw,size=50M tmpfs $PWD/target
sudo mount -t tmpfs -o rw,size=1G tmpfs $PWD/node_modules

# have to re-install, because node_modules is now empty
yarn