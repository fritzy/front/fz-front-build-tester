# CHANGELOG

<!--- next entry here -->

## 0.5.1
2021-10-14

### Fixes

- test no run install (7873ed1ccbf3277e7cfbde1960cfd44db0834338)

## 0.5.0
2021-10-04

### Features

- new test pipeline (ad388e28e711dc1873827bb2ce16d2eaa258c48f)

## 0.4.0
2021-10-04

### Features

- new test pipeline (436cfe92f7d4b40599eaa4eaddbe3f0973f8f510)

### Fixes

- allow install job in tags pipelines (bbc749b205aafc4d2980e931ce899495c852c486)
- new pipeline (d0e914dc181500d3f1c3f4d75de4d9e3f9a5ae0f)

## 0.3.1
2021-10-04

### Fixes

- allow install job in tags pipelines (bbc749b205aafc4d2980e931ce899495c852c486)
- new pipeline (d0e914dc181500d3f1c3f4d75de4d9e3f9a5ae0f)

## 0.3.0
2021-09-25

### Features

- routes in sh format (38e800f636e11f0fcb8948e69e9e85bf99b75a0e)

## 0.2.0
2021-09-25

### Features

- add deploy configuration (98bc1767e6e3748d66ffb2911783d57fff694f1f)

## 0.1.0
2021-09-25

### Features

- 1 (12d1b247f27a4891184239b59d59c9bc9c3610be)

### Fixes

- replace chrome by chromium (b3f3d7652dfe25094abfb29fa5eff5a97cd1e369)